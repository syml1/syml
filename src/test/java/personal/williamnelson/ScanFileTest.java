/*
 * Syml, a free and open source programming language.
 * Copyright (C) 2021 William Nelson
 * mailto: catboardbeta AT gmail DOT com
 * Syml is free software, licensed under MIT license. See LICENSE
 * for more information.
 *
 * Syml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

package personal.williamnelson;

// All test classes should be annotated with Test,
// so JUnit knows these aren't helper function,
// and are test functions.
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
// Scanner is used to read from the files,
// simply because of it's hasNext() function.
import java.util.Scanner;

// If these fail, they throw java.lang.AssertionError
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import static personal.williamnelson.ScanFile.validInfile;

public class ScanFileTest {

    // Used to reset System.out after tests
    private static final PrintStream stdout = System.out;

    // ---------------------------------------------
    //  validInfile(File, boolean, Logger, boolean)
    // ---------------------------------------------

    @Test
    public void testValidInfile1() throws IOException {
        // A file that doesn't exist, thus this text should fail.
        assertEquals(validInfile(new File("ThisFileDoesntExist"),
                false,
                null,
                false),
        1);
    }

    @Test
    public void testValidInfile2() throws IOException {
        // This should fail, as /etc/ is a directory, not a file.
        assertEquals(validInfile(new File("/etc/"),
                false,
                null,
                false),
        2);
    }

    @Test
    public void testValidInfile3() throws IOException {
        // unreadable is a file I have on my system, which has chmod -r ran on it.
        assertEquals(validInfile(new File("unreadablefile"),
                false,
                null,
                false),
                3);
    }

    // ------------------------------
    //  info(String, Logger.LogType)
    // ------------------------------

    @Test
    public void testInfo1() throws IOException {
        final String STRING_TO_COMPARE = "Testing to make sure this is here.";
        File newFile = new File("temptest");

        boolean success = newFile.createNewFile();
        if (!success) {
            fail();
        }

        System.setOut(new PrintStream("temptest", StandardCharsets.UTF_8));

        new ScanFile(new File("ThisDoesntMatter"),
                new File(""),
                true,
                false,
                new File("AlsoDoesntMatter"),
                true).info(STRING_TO_COMPARE, Logger.LogType.INFO);

        System.setOut(stdout);

        var s = new Scanner(new File("temptest"),
                StandardCharsets.UTF_8).useDelimiter("\\Z");

        Files.deleteIfExists(Paths.get("temptest"));

        assertEquals(s.next(), STRING_TO_COMPARE);
    }

    @Test
    public void testInfo2() throws IOException {

        final String STRING_TO_COMPARE = "Testing to make sure this is here.";
        File logfile = new File("log.log");

        boolean alreadyExists = logfile.exists();
        if (alreadyExists) {
            fail();
        }

        new ScanFile(new File("ThisDoesntMatter"),
                new File(""),
                true,
                true,
                logfile,
                true).info(STRING_TO_COMPARE, Logger.LogType.INFO);

        var s = new Scanner(logfile,
                StandardCharsets.UTF_8);
        StringBuilder fullFileSB = new StringBuilder();
        while (s.hasNext()) {
            fullFileSB.append(s.next());
        }
        String fullFile = fullFileSB.toString();
        s.close();

        Files.deleteIfExists(Paths.get(logfile.toString()));

        assertTrue(true);
    }
}
