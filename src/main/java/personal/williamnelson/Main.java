/*
 * Syml, a free and open source programming language.
 * Copyright (C) 2021 William Nelson
 * mailto: catboardbeta AT gmail DOT com
 *
 * Syml is free software, licensed under MIT license. See LICENSE 
 * for more information.
 *
 * Syml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

package personal.williamnelson;

// Picocli is used for command argument parsing,
// so I didn't have to write my own library.
// https://picocli.info/
// for more information.
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.io.*;
// Any application using Picocli must implement either Callable or Runnable.
import java.util.concurrent.Callable;

@Command(name = "Syml",
        description = "Compile & run a Syml file.",
        mixinStandardHelpOptions = true,
        version = "Syml 1.0",
        sortOptions = false)
public class Main implements Callable<Integer> {

    @Option(names = {"-v", "--verbose"}, description = "Shows information during compile.")
    public boolean verbose;

    // An input file is always required (unless -V (version) or -h (help) options are used,
    // and because @Command's mixinStandardHelpOptions = true, that is automatically handled
    // by Picocli.
    @Option(names = {"-i", "--in", "--input"}, description = "File to compile", required = true)
    public File input;

    @Option(names = {"-o", "--out", "--output"}, description = "File to output", defaultValue = "out.symo")
    public File output;

    // I might want to change how I determine if there just shouldn't be a logger,
    // as what if someone wanted their logfile to be named "NOLOGGER"?
    @Option(names = {"-l", "--log"}, description = "File to output logging information to", defaultValue = "NOLOGGER")
    public File logfile;

    // In a Picocli application, call() is where you should have any app logic.
    // main(String[] args) is only used to start Picocli.
    @Override
    public Integer call() throws Exception {
        boolean log = !("NOLOGGER".equals(logfile.getName()));

        // Scan the file, and return it's exitcode.
        new ScanFile(input, output, verbose, log, logfile);
        return 0;
    }

    public static void main(String[] args) {
        // Start Picocli.
        int exitCode = new CommandLine(new Main()).execute(args);
        System.exit(exitCode);
    }
}
