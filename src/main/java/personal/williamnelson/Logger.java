/*
 * Syml, a free and open source programming language.
 * Copyright (C) 2021 William Nelson
 * mailto: catboardbeta AT gmail DOT com
 * Syml is free software, licensed under MIT license. See LICENSE
 * for more information.
 *
 * Syml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

package personal.williamnelson;

import java.io.*; //NOSONAR
// Currently only using UTF_8, to be changed though.
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects; // Objects.requireNonNull(Object)

// Closeable is implemted so it is automatically closed in try-with-resources.
public class Logger implements Closeable {

    private final File logfile;

    // When logging, you must specify if it is an INFO, WARN, ERROR, or FATAL log message.
    public enum LogType {
        FATAL, ERROR, WARN, INFO
    }

    // fos and osw are declared outside of init(File) function so they can be used in log(...) functions.
    protected FileOutputStream fos;
    protected OutputStreamWriter osw;

    // example: 2021/27/5 12:07:23
    protected DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

    // Logger class is effectively useless until this function is ran.
    public Logger(File fileToLog, boolean clearFirst) {
        try {
            // If the file already exists, delete it's contents.
            if (fileToLog.exists() && clearFirst) {
                clearLogFile();

                // Otherwise, create the file.
            } else if(!fileToLog.createNewFile()) {
                    System.out.println("An unrecognized error occurred while creating the logfile.");
            } else {
                // This block shouldn't ever be entered,
                // as it should either go into the if, else if, or catch block.
            }

        } catch (IOException e) { //NOSONAR
            System.out.println("An error occurred while creating a logfile. Are you sure that '" +
                                fileToLog +
                               "' is a valid filename?"); 
            System.exit(1);
        }
        
        try {
            // Instantiate the previously declared fos and osw.
            fos = new FileOutputStream(fileToLog, true);
            osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
        } catch (IOException e) {
            // This should only happen if Logger loses permission to edit the logfile or if it is deleted.
            System.out.println("An unrecognized error occurred while start logging stream and " +
                               "writing to log stream, despite passing all tests.");
            System.exit(1);
        }
        this.logfile = fileToLog;
    }

    public void log(Object o, LogType logType) throws IOException {
        // logType should be 0 = INFO, 1 = WARN, 2 = ERROR, 3 = FATAL
        osw.write(getTime() + ": ");
        osw.write(String.valueOf(logType));
        osw.write(o + "\n");
    }

    // This will only ever be used in very specific cases, such as if this was to be logged:
    // Foo count:
    // 23
    // Bar count:
    // 42
    // But usually it would be formatted as
    // Foo count: 23
    // Bar count: 42
    // Second is prefered, this function should be used sparingly
    public void log(int i, LogType logType) throws IOException {
        log(Integer.valueOf(i), logType);
    }

    // Simply gets the current time in UTC
    private String getTime() {
        LocalDateTime ldtNow = LocalDateTime.now();
        return ldtNow.format(dtf);
    }

    private void clearLogFile() throws IOException {
        OutputStreamWriter oswClearer = null;
        try {
            // I cannot use the same fos or osw, as the clearing function
            // functions by overwriting the file with ""
            FileOutputStream fosClearer = new FileOutputStream(logfile, false);
            oswClearer = new OutputStreamWriter(fosClearer, StandardCharsets.UTF_8);
            oswClearer.write("");
        } catch (IOException e) {
            // This shouldn't happen unless the file is deleted midway
            System.err.println("FATAL: The file became unwritable partway through processing logfile, exiting.");
            System.exit(1);
        } finally {
            // We don't need this osw anymore
            Objects.requireNonNull(oswClearer).close();
        }

    }

    public void close() throws IOException {
        // Closing the wrapper OutputStreamWriter also closes the inner FileOutputStream
        osw.close();
    }
}
