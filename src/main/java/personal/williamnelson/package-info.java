/**
 *  * Syml, a free and open source programming language.
 * Copyright (C) 2021 William Nelson
 * mailto: catboardbeta AT gmail DOT com
 *
 * Syml is free software, licensed under MIT license. See LICENSE 
 * for more information.
 *
 * Syml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * Source code for Syml Compiler and Runner. Most values and functions are public, and wrapper applications can use and display these values.
**/

package personal.williamnelson;
