/*
 * Syml, a free and open source programming language.
 * Copyright (C) 2021 William Nelson
 * mailto: catboardbeta AT gmail DOT com
 *
 * Syml is free software, licensed under MIT license. See LICENSE 
 * for more information.
 *
 * Syml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

package personal.williamnelson;

import java.io.Closeable;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.IOException;


// LogType is an enum required for the Logger class
import personal.williamnelson.Logger.LogType;

// Implements Closeable for closing the logger (if used).
public class ScanFile implements Closeable {

    private static final String FILE  = "File '";
    private static final String FATAL = "FATAL: File'";

    private final boolean shouldLog;
    private final boolean verbose;

    public final Logger l;

    public ScanFile(File infile, File outfile, boolean verbose, boolean shouldLog, File logfile)
            throws IOException {
        if (shouldLog) {
            l = new Logger(logfile, true);
        } else {
            // If we don't need the logger, we can ignore it.
            l = null;
        }

        // These class-specific variables are used in
        // other methods, so it knows it if should log
        // and if it should be verbose.
        this.shouldLog = shouldLog;
        this.verbose = verbose;

        if (shouldLog) {
            l.log("Starting the log", LogType.INFO);
        }

        // Check if the input file is valid, if not validInfine will call System.exit()
        validInfile(infile, shouldLog, l, true);

        // info(String, LogType) prints String s to the screen if self.verbose = true,
        // and logs String s with severity LogType logType.
        info("Beginning compilation...", LogType.INFO);

        // This should be changed in the future, as it means that if a user inputs
        // output = "out.symo", than this message will be displayed and logged.
        if ("out.symo".equals(outfile.toString())) {
            info("Output file not specified, defaulting to '" + outfile + "'.", LogType.WARN);
        }

    }

    public ScanFile(File infile, File outfile, boolean verbose, boolean shouldLog, File logfile, boolean setAsTrue) {
        // THIS SHOULD ONLY BE USED IN TESTS!

        this.shouldLog = shouldLog;
        this.verbose = verbose;

        if (!setAsTrue) {
            System.exit(0);
        }

        if (shouldLog) {
            l = new Logger(logfile, true);
        } else {
            l = null;
        }
    }

    public static int validInfile(File in, boolean shouldLog, Logger logger, boolean shouldExit)
            throws IOException {
        // Count of times to print the FATAL message in the logfile
        final int LOOPCOUNT = 5;
        final int RETVAL;
        // If the input file is a directory, it cannot be properly read.
         if (in.isDirectory()) {

             if (shouldLog) {
                 for (int i = LOOPCOUNT; i > 0; i--) {
                     logger.log(FILE + in + "' is a directory.", LogType.FATAL);
                 }
                 System.out.println(FATAL + in + "' is a directory.");
             }
             if (shouldExit) {
                 System.exit(1);
             }

             RETVAL = 2;

             // If the input file doesn't exist, we cannot proceed.
         } else if (!Files.exists(Paths.get(in.getAbsolutePath()))) {

            if (shouldLog) {
                for (int i = LOOPCOUNT; i > 0; i--) {
                    logger.log(FILE + in + "' doesn't exist.", LogType.FATAL );
                }
                System.out.println(FATAL + in + "' doesn't exist.");
            }
            if (shouldExit) {
                System.exit(1);
            }

            RETVAL = 1;


        // Unreadable files are unreadable.
        } else if (!in.canRead()) {

            if (shouldLog) {
                for (int i = LOOPCOUNT; i > 0; i--) {
                    logger.log(FILE + in + "' cannot be read. Are you sure you have permission?", LogType.FATAL);
                }
                System.out.println(FATAL + in + "' cannot be read. Are you sure you have permission?");
            }
            if (shouldExit) {
                System.exit(1);
            }

            RETVAL = 3;

        } else {
            // Infile is valid, thus we can just exit.
            RETVAL = 0;
        }
         return RETVAL;
    }

    public final void info(String s, LogType logType) throws IOException {
        if (this.verbose) {
            System.out.println(s);
        }
        if (this.shouldLog) {
            l.log(s, logType);
        }
    }

    @Override
    public void close() throws IOException {
        l.close();
    }
}
